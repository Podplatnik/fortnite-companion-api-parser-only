
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CurrP2 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating___ trnRating;
    @SerializedName("score")
    @Expose
    private Score___ score;
    @SerializedName("top1")
    @Expose
    private Top1___ top1;
    @SerializedName("top3")
    @Expose
    private Top3___ top3;
    @SerializedName("top5")
    @Expose
    private Top5___ top5;
    @SerializedName("top6")
    @Expose
    private Top6___ top6;
    @SerializedName("top10")
    @Expose
    private Top10___ top10;
    @SerializedName("top12")
    @Expose
    private Top12___ top12;
    @SerializedName("top25")
    @Expose
    private Top25___ top25;
    @SerializedName("kd")
    @Expose
    private Kd___ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio___ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches___ matches;
    @SerializedName("kills")
    @Expose
    private Kills___ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg___ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch___ scorePerMatch;
    public final static Parcelable.Creator<CurrP2> CREATOR = new Creator<CurrP2>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CurrP2 createFromParcel(Parcel in) {
            return new CurrP2(in);
        }

        public CurrP2 [] newArray(int size) {
            return (new CurrP2[size]);
        }

    }
    ;

    protected CurrP2(Parcel in) {
        this.trnRating = ((TrnRating___) in.readValue((TrnRating___.class.getClassLoader())));
        this.score = ((Score___) in.readValue((Score___.class.getClassLoader())));
        this.top1 = ((Top1___) in.readValue((Top1___.class.getClassLoader())));
        this.top3 = ((Top3___) in.readValue((Top3___.class.getClassLoader())));
        this.top5 = ((Top5___) in.readValue((Top5___.class.getClassLoader())));
        this.top6 = ((Top6___) in.readValue((Top6___.class.getClassLoader())));
        this.top10 = ((Top10___) in.readValue((Top10___.class.getClassLoader())));
        this.top12 = ((Top12___) in.readValue((Top12___.class.getClassLoader())));
        this.top25 = ((Top25___) in.readValue((Top25___.class.getClassLoader())));
        this.kd = ((Kd___) in.readValue((Kd___.class.getClassLoader())));
        this.winRatio = ((WinRatio___) in.readValue((WinRatio___.class.getClassLoader())));
        this.matches = ((Matches___) in.readValue((Matches___.class.getClassLoader())));
        this.kills = ((Kills___) in.readValue((Kills___.class.getClassLoader())));
        this.kpg = ((Kpg___) in.readValue((Kpg___.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch___) in.readValue((ScorePerMatch___.class.getClassLoader())));
    }

    public CurrP2() {
    }

    public TrnRating___ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating___ trnRating) {
        this.trnRating = trnRating;
    }

    public Score___ getScore() {
        return score;
    }

    public void setScore(Score___ score) {
        this.score = score;
    }

    public Top1___ getTop1() {
        return top1;
    }

    public void setTop1(Top1___ top1) {
        this.top1 = top1;
    }

    public Top3___ getTop3() {
        return top3;
    }

    public void setTop3(Top3___ top3) {
        this.top3 = top3;
    }

    public Top5___ getTop5() {
        return top5;
    }

    public void setTop5(Top5___ top5) {
        this.top5 = top5;
    }

    public Top6___ getTop6() {
        return top6;
    }

    public void setTop6(Top6___ top6) {
        this.top6 = top6;
    }

    public Top10___ getTop10() {
        return top10;
    }

    public void setTop10(Top10___ top10) {
        this.top10 = top10;
    }

    public Top12___ getTop12() {
        return top12;
    }

    public void setTop12(Top12___ top12) {
        this.top12 = top12;
    }

    public Top25___ getTop25() {
        return top25;
    }

    public void setTop25(Top25___ top25) {
        this.top25 = top25;
    }

    public Kd___ getKd() {
        return kd;
    }

    public void setKd(Kd___ kd) {
        this.kd = kd;
    }

    public WinRatio___ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio___ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches___ getMatches() {
        return matches;
    }

    public void setMatches(Matches___ matches) {
        this.matches = matches;
    }

    public Kills___ getKills() {
        return kills;
    }

    public void setKills(Kills___ kills) {
        this.kills = kills;
    }

    public Kpg___ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg___ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch___ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch___ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
