package um.feri.rvir.fortnitecompanionproject.parserOrodja;

/**
 * Created by Miha on 16/06/2018.
 */

public class IgralecSimple {

    public IgralecSimple() {
    }

    public IgralecSimple(String accountId, Integer platformId, String platformName, String platformNameLong, String epicUserHandle, Integer soloTRNRating, Integer soloTRNRatingRank, Integer soloScore, Integer soloScoreRank, Integer soloWins, Integer soloWinsRank, Integer soloTop10, Integer soloTop25, Double soloKillDeath, Integer soloKillDeathRank, Double soloWinRadio, Integer soloWinRadioRank, Integer soloMatches, Integer soloKills, Integer soloKillsRank, Double soloKillsPerMatch, Integer soloKillsPerMatchRank, Double soloScorePerMatch, Integer soloScorePerMatchRank, Integer duoTRNRating, Integer duoTRNRatingRank, Integer duoScore, Integer duoScoreRank, Integer duoWins, Integer duoWinsRank, Integer duoTop5, Integer duoTop12, Double duoKillDeath, Integer duoKillDeathRank, Double duoWinRadio, Integer duoWinRadioRank, Integer duoMatches, Integer duoKills, Integer duoKillsRank, Double duoKillsPerMatch, Integer duoKillsPerMatchRank, Double duoScorePerMatch, Integer duoScorePerMatchRank, Integer squadTRNRating, Integer squadTRNRatingRank, Integer squadScore, Integer squadScoreRank, Integer squadWins, Integer squadWinsRank, Integer squadTop3, Integer squadTop6, Double squadKillDeath, Integer squadKillDeathRank, Double squadWinRadio, Integer squadWinRadioRank, Integer squadMatches, Integer squadKills, Integer squadKillsRank, Double squadKillsPerMatch, Integer squadKillsPerMatchRank, Double squadScorePerMatch, Integer squadScorePerMatchRank, String lifeTimeScore, String lifeTimeMatchesPlayed, String lifeTimeWins, String lifeTimeWinsProcent, String lifeTimeKills, String lifeTimeKillDeath) {
        this.accountId = accountId;
        this.platformId = platformId;
        this.platformName = platformName;
        this.platformNameLong = platformNameLong;
        this.epicUserHandle = epicUserHandle;
        this.soloTRNRating = soloTRNRating;
        this.soloTRNRatingRank = soloTRNRatingRank;
        this.soloScore = soloScore;
        this.soloScoreRank = soloScoreRank;
        this.soloWins = soloWins;
        this.soloWinsRank = soloWinsRank;
        this.soloTop10 = soloTop10;
        this.soloTop25 = soloTop25;
        this.soloKillDeath = soloKillDeath;
        this.soloKillDeathRank = soloKillDeathRank;
        this.soloWinRatio = soloWinRadio;
        this.soloWinRatioRank = soloWinRadioRank;
        this.soloMatches = soloMatches;
        this.soloKills = soloKills;
        this.soloKillsRank = soloKillsRank;
        this.soloKillsPerMatch = soloKillsPerMatch;
        this.soloKillsPerMatchRank = soloKillsPerMatchRank;
        this.soloScorePerMatch = soloScorePerMatch;
        this.soloScorePerMatchRank = soloScorePerMatchRank;
        this.duoTRNRating = duoTRNRating;
        this.duoTRNRatingRank = duoTRNRatingRank;
        this.duoScore = duoScore;
        this.duoScoreRank = duoScoreRank;
        this.duoWins = duoWins;
        this.duoWinsRank = duoWinsRank;
        this.duoTop5 = duoTop5;
        this.duoTop12 = duoTop12;
        this.duoKillDeath = duoKillDeath;
        this.duoKillDeathRank = duoKillDeathRank;
        this.duoWinRatio = duoWinRadio;
        this.duoWinRatioRank = duoWinRadioRank;
        this.duoMatches = duoMatches;
        this.duoKills = duoKills;
        this.duoKillsRank = duoKillsRank;
        this.duoKillsPerMatch = duoKillsPerMatch;
        this.duoKillsPerMatchRank = duoKillsPerMatchRank;
        this.duoScorePerMatch = duoScorePerMatch;
        this.duoScorePerMatchRank = duoScorePerMatchRank;
        this.squadTRNRating = squadTRNRating;
        this.squadTRNRatingRank = squadTRNRatingRank;
        this.squadScore = squadScore;
        this.squadScoreRank = squadScoreRank;
        this.squadWins = squadWins;
        this.squadWinsRank = squadWinsRank;
        this.squadTop3 = squadTop3;
        this.squadTop6 = squadTop6;
        this.squadKillDeath = squadKillDeath;
        this.squadKillDeathRank = squadKillDeathRank;
        this.squadWinRatio = squadWinRadio;
        this.squadWinRatioRank = squadWinRadioRank;
        this.squadMatches = squadMatches;
        this.squadKills = squadKills;
        this.squadKillsRank = squadKillsRank;
        this.squadKillsPerMatch = squadKillsPerMatch;
        this.squadKillsPerMatchRank = squadKillsPerMatchRank;
        this.squadScorePerMatch = squadScorePerMatch;
        this.squadScorePerMatchRank = squadScorePerMatchRank;
        this.lifeTimeScore = lifeTimeScore;
        this.lifeTimeMatchesPlayed = lifeTimeMatchesPlayed;
        this.lifeTimeWins = lifeTimeWins;
        this.lifeTimeWinsProcent = lifeTimeWinsProcent;
        this.lifeTimeKills = lifeTimeKills;
        this.lifeTimeKillDeath = lifeTimeKillDeath;
    }

    //OSNOVE
    private String accountId;
    private Integer platformId;
    private String platformName;
    private String platformNameLong;
    private String epicUserHandle;

    //SOLO
    private Integer soloTRNRating;
    private Integer soloTRNRatingRank;
    private Integer soloScore;
    private Integer soloScoreRank;
    private Integer soloWins;
    private Integer soloWinsRank;
    private Integer soloTop10;
    private Integer soloTop25;
    private Double soloKillDeath;
    private Integer soloKillDeathRank;
    private Double soloWinRatio;
    private Integer soloWinRatioRank;
    private Integer soloMatches;
    private Integer soloKills;
    private Integer soloKillsRank;
    private Double soloKillsPerMatch;
    private Integer soloKillsPerMatchRank;
    private Double soloScorePerMatch;
    private Integer soloScorePerMatchRank;

    //DUO
    private Integer duoTRNRating;
    private Integer duoTRNRatingRank;
    private Integer duoScore;
    private Integer duoScoreRank;
    private Integer duoWins;
    private Integer duoWinsRank;
    private Integer duoTop5;
    private Integer duoTop12;
    private Double duoKillDeath;
    private Integer duoKillDeathRank;
    private Double duoWinRatio;
    private Integer duoWinRatioRank;
    private Integer duoMatches;
    private Integer duoKills;
    private Integer duoKillsRank;
    private Double duoKillsPerMatch;
    private Integer duoKillsPerMatchRank;
    private Double duoScorePerMatch;
    private Integer duoScorePerMatchRank;

    //SQUAD
    private Integer squadTRNRating;
    private Integer squadTRNRatingRank;
    private Integer squadScore;
    private Integer squadScoreRank;
    private Integer squadWins;
    private Integer squadWinsRank;
    private Integer squadTop3;
    private Integer squadTop6;
    private Double squadKillDeath;
    private Integer squadKillDeathRank;
    private Double squadWinRatio;
    private Integer squadWinRatioRank;
    private Integer squadMatches;
    private Integer squadKills;
    private Integer squadKillsRank;
    private Double squadKillsPerMatch;
    private Integer squadKillsPerMatchRank;
    private Double squadScorePerMatch;
    private Integer squadScorePerMatchRank;

    //LIFETIME
    private String lifeTimeScore;
    private String lifeTimeMatchesPlayed;
    private String lifeTimeWins;
    private String lifeTimeWinsProcent;
    private String lifeTimeKills;
    private String lifeTimeKillDeath;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Integer platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformNameLong() {
        return platformNameLong;
    }

    public void setPlatformNameLong(String platformNameLong) {
        this.platformNameLong = platformNameLong;
    }

    public String getEpicUserHandle() {
        return epicUserHandle;
    }

    public void setEpicUserHandle(String epicUserHandle) {
        this.epicUserHandle = epicUserHandle;
    }

    public Integer getSoloTRNRating() {
        return soloTRNRating;
    }

    public void setSoloTRNRating(Integer soloTRNRating) {
        this.soloTRNRating = soloTRNRating;
    }

    public Integer getSoloTRNRatingRank() {
        return soloTRNRatingRank;
    }

    public void setSoloTRNRatingRank(Integer soloTRNRatingRank) {
        this.soloTRNRatingRank = soloTRNRatingRank;
    }

    public Integer getSoloScore() {
        return soloScore;
    }

    public void setSoloScore(Integer soloScore) {
        this.soloScore = soloScore;
    }

    public Integer getSoloScoreRank() {
        return soloScoreRank;
    }

    public void setSoloScoreRank(Integer soloScoreRank) {
        this.soloScoreRank = soloScoreRank;
    }

    public Integer getSoloWins() {
        return soloWins;
    }

    public void setSoloWins(Integer soloWins) {
        this.soloWins = soloWins;
    }

    public Integer getSoloWinsRank() {
        return soloWinsRank;
    }

    public void setSoloWinsRank(Integer soloWinsRank) {
        this.soloWinsRank = soloWinsRank;
    }

    public Integer getSoloTop10() {
        return soloTop10;
    }

    public void setSoloTop10(Integer soloTop10) {
        this.soloTop10 = soloTop10;
    }

    public Integer getSoloTop25() {
        return soloTop25;
    }

    public void setSoloTop25(Integer soloTop25) {
        this.soloTop25 = soloTop25;
    }

    public Double getSoloKillDeath() {
        return soloKillDeath;
    }

    public void setSoloKillDeath(Double soloKillDeath) {
        this.soloKillDeath = soloKillDeath;
    }

    public Integer getSoloKillDeathRank() {
        return soloKillDeathRank;
    }

    public void setSoloKillDeathRank(Integer soloKillDeathRank) {
        this.soloKillDeathRank = soloKillDeathRank;
    }

    public Double getSoloWinRatio() {
        return soloWinRatio;
    }

    public void setSoloWinRatio(Double soloWinRatio) {
        this.soloWinRatio = soloWinRatio;
    }

    public Integer getSoloWinRatioRank() {
        return soloWinRatioRank;
    }

    public void setSoloWinRatioRank(Integer soloWinRatioRank) {
        this.soloWinRatioRank = soloWinRatioRank;
    }

    public Integer getSoloMatches() {
        return soloMatches;
    }

    public void setSoloMatches(Integer soloMatches) {
        this.soloMatches = soloMatches;
    }

    public Integer getSoloKills() {
        return soloKills;
    }

    public void setSoloKills(Integer soloKills) {
        this.soloKills = soloKills;
    }

    public Integer getSoloKillsRank() {
        return soloKillsRank;
    }

    public void setSoloKillsRank(Integer soloKillsRank) {
        this.soloKillsRank = soloKillsRank;
    }

    public Double getSoloKillsPerMatch() {
        return soloKillsPerMatch;
    }

    public void setSoloKillsPerMatch(Double soloKillsPerMatch) {
        this.soloKillsPerMatch = soloKillsPerMatch;
    }

    public Integer getSoloKillsPerMatchRank() {
        return soloKillsPerMatchRank;
    }

    public void setSoloKillsPerMatchRank(Integer soloKillsPerMatchRank) {
        this.soloKillsPerMatchRank = soloKillsPerMatchRank;
    }

    public Double getSoloScorePerMatch() {
        return soloScorePerMatch;
    }

    public void setSoloScorePerMatch(Double soloScorePerMatch) {
        this.soloScorePerMatch = soloScorePerMatch;
    }

    public Integer getSoloScorePerMatchRank() {
        return soloScorePerMatchRank;
    }

    public void setSoloScorePerMatchRank(Integer soloScorePerMatchRank) {
        this.soloScorePerMatchRank = soloScorePerMatchRank;
    }

    public Integer getDuoTRNRating() {
        return duoTRNRating;
    }

    public void setDuoTRNRating(Integer duoTRNRating) {
        this.duoTRNRating = duoTRNRating;
    }

    public Integer getDuoTRNRatingRank() {
        return duoTRNRatingRank;
    }

    public void setDuoTRNRatingRank(Integer duoTRNRatingRank) {
        this.duoTRNRatingRank = duoTRNRatingRank;
    }

    public Integer getDuoScore() {
        return duoScore;
    }

    public void setDuoScore(Integer duoScore) {
        this.duoScore = duoScore;
    }

    public Integer getDuoScoreRank() {
        return duoScoreRank;
    }

    public void setDuoScoreRank(Integer duoScoreRank) {
        this.duoScoreRank = duoScoreRank;
    }

    public Integer getDuoWins() {
        return duoWins;
    }

    public void setDuoWins(Integer duoWins) {
        this.duoWins = duoWins;
    }

    public Integer getDuoWinsRank() {
        return duoWinsRank;
    }

    public void setDuoWinsRank(Integer duoWinsRank) {
        this.duoWinsRank = duoWinsRank;
    }

    public Integer getDuoTop5() {
        return duoTop5;
    }

    public void setDuoTop5(Integer duoTop5) {
        this.duoTop5 = duoTop5;
    }

    public Integer getDuoTop12() {
        return duoTop12;
    }

    public void setDuoTop12(Integer duoTop12) {
        this.duoTop12 = duoTop12;
    }

    public Double getDuoKillDeath() {
        return duoKillDeath;
    }

    public void setDuoKillDeath(Double duoKillDeath) {
        this.duoKillDeath = duoKillDeath;
    }

    public Integer getDuoKillDeathRank() {
        return duoKillDeathRank;
    }

    public void setDuoKillDeathRank(Integer duoKillDeathRank) {
        this.duoKillDeathRank = duoKillDeathRank;
    }

    public Double getDuoWinRatio() {
        return duoWinRatio;
    }

    public void setDuoWinRatio(Double duoWinRatio) {
        this.duoWinRatio = duoWinRatio;
    }

    public Integer getDuoWinRatioRank() {
        return duoWinRatioRank;
    }

    public void setDuoWinRatioRank(Integer duoWinRatioRank) {
        this.duoWinRatioRank = duoWinRatioRank;
    }

    public Integer getDuoMatches() {
        return duoMatches;
    }

    public void setDuoMatches(Integer duoMatches) {
        this.duoMatches = duoMatches;
    }

    public Integer getDuoKills() {
        return duoKills;
    }

    public void setDuoKills(Integer duoKills) {
        this.duoKills = duoKills;
    }

    public Integer getDuoKillsRank() {
        return duoKillsRank;
    }

    public void setDuoKillsRank(Integer duoKillsRank) {
        this.duoKillsRank = duoKillsRank;
    }

    public Double getDuoKillsPerMatch() {
        return duoKillsPerMatch;
    }

    public void setDuoKillsPerMatch(Double duoKillsPerMatch) {
        this.duoKillsPerMatch = duoKillsPerMatch;
    }

    public Integer getDuoKillsPerMatchRank() {
        return duoKillsPerMatchRank;
    }

    public void setDuoKillsPerMatchRank(Integer duoKillsPerMatchRank) {
        this.duoKillsPerMatchRank = duoKillsPerMatchRank;
    }

    public Double getDuoScorePerMatch() {
        return duoScorePerMatch;
    }

    public void setDuoScorePerMatch(Double duoScorePerMatch) {
        this.duoScorePerMatch = duoScorePerMatch;
    }

    public Integer getDuoScorePerMatchRank() {
        return duoScorePerMatchRank;
    }

    public void setDuoScorePerMatchRank(Integer duoScorePerMatchRank) {
        this.duoScorePerMatchRank = duoScorePerMatchRank;
    }

    public Integer getSquadTRNRating() {
        return squadTRNRating;
    }

    public void setSquadTRNRating(Integer squadTRNRating) {
        this.squadTRNRating = squadTRNRating;
    }

    public Integer getSquadTRNRatingRank() {
        return squadTRNRatingRank;
    }

    public void setSquadTRNRatingRank(Integer squadTRNRatingRank) {
        this.squadTRNRatingRank = squadTRNRatingRank;
    }

    public Integer getSquadScore() {
        return squadScore;
    }

    public void setSquadScore(Integer squadScore) {
        this.squadScore = squadScore;
    }

    public Integer getSquadScoreRank() {
        return squadScoreRank;
    }

    public void setSquadScoreRank(Integer squadScoreRank) {
        this.squadScoreRank = squadScoreRank;
    }

    public Integer getSquadWins() {
        return squadWins;
    }

    public void setSquadWins(Integer squadWins) {
        this.squadWins = squadWins;
    }

    public Integer getSquadWinsRank() {
        return squadWinsRank;
    }

    public void setSquadWinsRank(Integer squadWinsRank) {
        this.squadWinsRank = squadWinsRank;
    }

    public Integer getSquadTop3() {
        return squadTop3;
    }

    public void setSquadTop3(Integer squadTop3) {
        this.squadTop3 = squadTop3;
    }

    public Integer getSquadTop6() {
        return squadTop6;
    }

    public void setSquadTop6(Integer squadTop6) {
        this.squadTop6 = squadTop6;
    }

    public Double getSquadKillDeath() {
        return squadKillDeath;
    }

    public void setSquadKillDeath(Double squadKillDeath) {
        this.squadKillDeath = squadKillDeath;
    }

    public Integer getSquadKillDeathRank() {
        return squadKillDeathRank;
    }

    public void setSquadKillDeathRank(Integer squadKillDeathRank) {
        this.squadKillDeathRank = squadKillDeathRank;
    }

    public Double getSquadWinRatio() {
        return squadWinRatio;
    }

    public void setSquadWinRatio(Double squadWinRatio) {
        this.squadWinRatio = squadWinRatio;
    }

    public Integer getSquadWinRatioRank() {
        return squadWinRatioRank;
    }

    public void setSquadWinRatioRank(Integer squadWinRatioRank) {
        this.squadWinRatioRank = squadWinRatioRank;
    }

    public Integer getSquadMatches() {
        return squadMatches;
    }

    public void setSquadMatches(Integer squadMatches) {
        this.squadMatches = squadMatches;
    }

    public Integer getSquadKills() {
        return squadKills;
    }

    public void setSquadKills(Integer squadKills) {
        this.squadKills = squadKills;
    }

    public Integer getSquadKillsRank() {
        return squadKillsRank;
    }

    public void setSquadKillsRank(Integer squadKillsRank) {
        this.squadKillsRank = squadKillsRank;
    }

    public Double getSquadKillsPerMatch() {
        return squadKillsPerMatch;
    }

    public void setSquadKillsPerMatch(Double squadKillsPerMatch) {
        this.squadKillsPerMatch = squadKillsPerMatch;
    }

    public Integer getSquadKillsPerMatchRank() {
        return squadKillsPerMatchRank;
    }

    public void setSquadKillsPerMatchRank(Integer squadKillsPerMatchRank) {
        this.squadKillsPerMatchRank = squadKillsPerMatchRank;
    }

    public Double getSquadScorePerMatch() {
        return squadScorePerMatch;
    }

    public void setSquadScorePerMatch(Double squadScorePerMatch) {
        this.squadScorePerMatch = squadScorePerMatch;
    }

    public Integer getSquadScorePerMatchRank() {
        return squadScorePerMatchRank;
    }

    public void setSquadScorePerMatchRank(Integer squadScorePerMatchRank) {
        this.squadScorePerMatchRank = squadScorePerMatchRank;
    }

    public String getLifeTimeScore() {
        return lifeTimeScore;
    }

    public void setLifeTimeScore(String lifeTimeScore) {
        this.lifeTimeScore = lifeTimeScore;
    }

    public String getLifeTimeMatchesPlayed() {
        return lifeTimeMatchesPlayed;
    }

    public void setLifeTimeMatchesPlayed(String lifeTimeMatchesPlayed) {
        this.lifeTimeMatchesPlayed = lifeTimeMatchesPlayed;
    }

    public String getLifeTimeWins() {
        return lifeTimeWins;
    }

    public void setLifeTimeWins(String lifeTimeWins) {
        this.lifeTimeWins = lifeTimeWins;
    }

    public String getLifeTimeWinsProcent() {
        return lifeTimeWinsProcent;
    }

    public void setLifeTimeWinsProcent(String lifeTimeWinsProcent) {
        this.lifeTimeWinsProcent = lifeTimeWinsProcent;
    }

    public String getLifeTimeKills() {
        return lifeTimeKills;
    }

    public void setLifeTimeKills(String lifeTimeKills) {
        this.lifeTimeKills = lifeTimeKills;
    }

    public String getLifeTimeKillDeath() {
        return lifeTimeKillDeath;
    }

    public void setLifeTimeKillDeath(String lifeTimeKillDeath) {
        this.lifeTimeKillDeath = lifeTimeKillDeath;
    }

    @Override
    public String toString() {
        return "IgralecSimple{" +
                "accountId='" + accountId + '\'' +
                ", platformId=" + platformId +
                ", platformName='" + platformName + '\'' +
                ", platformNameLong='" + platformNameLong + '\'' +
                ", epicUserHandle='" + epicUserHandle + '\'' +
                ", soloTRNRating=" + soloTRNRating +
                ", soloTRNRatingRank=" + soloTRNRatingRank +
                ", soloScore=" + soloScore +
                ", soloScoreRank=" + soloScoreRank +
                ", soloWins=" + soloWins +
                ", soloWinsRank=" + soloWinsRank +
                ", soloTop10=" + soloTop10 +
                ", soloTop25=" + soloTop25 +
                ", soloKillDeath=" + soloKillDeath +
                ", soloKillDeathRank=" + soloKillDeathRank +
                ", soloWinRatio=" + soloWinRatio +
                ", soloWinRatioRank=" + soloWinRatioRank +
                ", soloMatches=" + soloMatches +
                ", soloKills=" + soloKills +
                ", soloKillsRank=" + soloKillsRank +
                ", soloKillsPerMatch=" + soloKillsPerMatch +
                ", soloKillsPerMatchRank=" + soloKillsPerMatchRank +
                ", soloScorePerMatch=" + soloScorePerMatch +
                ", soloScorePerMatchRank=" + soloScorePerMatchRank +
                ", duoTRNRating=" + duoTRNRating +
                ", duoTRNRatingRank=" + duoTRNRatingRank +
                ", duoScore=" + duoScore +
                ", duoScoreRank=" + duoScoreRank +
                ", duoWins=" + duoWins +
                ", duoWinsRank=" + duoWinsRank +
                ", duoTop5=" + duoTop5 +
                ", duoTop12=" + duoTop12 +
                ", duoKillDeath=" + duoKillDeath +
                ", duoKillDeathRank=" + duoKillDeathRank +
                ", duoWinRatio=" + duoWinRatio +
                ", duoWinRatioRank=" + duoWinRatioRank +
                ", duoMatches=" + duoMatches +
                ", duoKills=" + duoKills +
                ", duoKillsRank=" + duoKillsRank +
                ", duoKillsPerMatch=" + duoKillsPerMatch +
                ", duoKillsPerMatchRank=" + duoKillsPerMatchRank +
                ", duoScorePerMatch=" + duoScorePerMatch +
                ", duoScorePerMatchRank=" + duoScorePerMatchRank +
                ", squadTRNRating=" + squadTRNRating +
                ", squadTRNRatingRank=" + squadTRNRatingRank +
                ", squadScore=" + squadScore +
                ", squadScoreRank=" + squadScoreRank +
                ", squadWins=" + squadWins +
                ", squadWinsRank=" + squadWinsRank +
                ", squadTop3=" + squadTop3 +
                ", squadTop6=" + squadTop6 +
                ", squadKillDeath=" + squadKillDeath +
                ", squadKillDeathRank=" + squadKillDeathRank +
                ", squadWinRatio=" + squadWinRatio +
                ", squadWinRatioRank=" + squadWinRatioRank +
                ", squadMatches=" + squadMatches +
                ", squadKills=" + squadKills +
                ", squadKillsRank=" + squadKillsRank +
                ", squadKillsPerMatch=" + squadKillsPerMatch +
                ", squadKillsPerMatchRank=" + squadKillsPerMatchRank +
                ", squadScorePerMatch=" + squadScorePerMatch +
                ", squadScorePerMatchRank=" + squadScorePerMatchRank +
                ", lifeTimeScore='" + lifeTimeScore + '\'' +
                ", lifeTimeMatchesPlayed='" + lifeTimeMatchesPlayed + '\'' +
                ", lifeTimeWins='" + lifeTimeWins + '\'' +
                ", lifeTimeWinsProcent='" + lifeTimeWinsProcent + '\'' +
                ", lifeTimeKills='" + lifeTimeKills + '\'' +
                ", lifeTimeKillDeath='" + lifeTimeKillDeath + '\'' +
                '}';
    }
}
