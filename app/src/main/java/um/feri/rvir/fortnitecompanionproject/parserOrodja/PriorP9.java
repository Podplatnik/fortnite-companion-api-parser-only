
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PriorP9 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating________ trnRating;
    @SerializedName("score")
    @Expose
    private Score________ score;
    @SerializedName("top1")
    @Expose
    private Top1________ top1;
    @SerializedName("top3")
    @Expose
    private Top3________ top3;
    @SerializedName("top5")
    @Expose
    private Top5________ top5;
    @SerializedName("top6")
    @Expose
    private Top6________ top6;
    @SerializedName("top10")
    @Expose
    private Top10________ top10;
    @SerializedName("top12")
    @Expose
    private Top12________ top12;
    @SerializedName("top25")
    @Expose
    private Top25________ top25;
    @SerializedName("kd")
    @Expose
    private Kd________ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio________ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches________ matches;
    @SerializedName("kills")
    @Expose
    private Kills________ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg________ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch________ scorePerMatch;
    public final static Parcelable.Creator<PriorP9> CREATOR = new Creator<PriorP9>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PriorP9 createFromParcel(Parcel in) {
            return new PriorP9(in);
        }

        public PriorP9 [] newArray(int size) {
            return (new PriorP9[size]);
        }

    }
    ;

    protected PriorP9(Parcel in) {
        this.trnRating = ((TrnRating________) in.readValue((TrnRating________.class.getClassLoader())));
        this.score = ((Score________) in.readValue((Score________.class.getClassLoader())));
        this.top1 = ((Top1________) in.readValue((Top1________.class.getClassLoader())));
        this.top3 = ((Top3________) in.readValue((Top3________.class.getClassLoader())));
        this.top5 = ((Top5________) in.readValue((Top5________.class.getClassLoader())));
        this.top6 = ((Top6________) in.readValue((Top6________.class.getClassLoader())));
        this.top10 = ((Top10________) in.readValue((Top10________.class.getClassLoader())));
        this.top12 = ((Top12________) in.readValue((Top12________.class.getClassLoader())));
        this.top25 = ((Top25________) in.readValue((Top25________.class.getClassLoader())));
        this.kd = ((Kd________) in.readValue((Kd________.class.getClassLoader())));
        this.winRatio = ((WinRatio________) in.readValue((WinRatio________.class.getClassLoader())));
        this.matches = ((Matches________) in.readValue((Matches________.class.getClassLoader())));
        this.kills = ((Kills________) in.readValue((Kills________.class.getClassLoader())));
        this.kpg = ((Kpg________) in.readValue((Kpg________.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch________) in.readValue((ScorePerMatch________.class.getClassLoader())));
    }

    public PriorP9() {
    }

    public TrnRating________ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating________ trnRating) {
        this.trnRating = trnRating;
    }

    public Score________ getScore() {
        return score;
    }

    public void setScore(Score________ score) {
        this.score = score;
    }

    public Top1________ getTop1() {
        return top1;
    }

    public void setTop1(Top1________ top1) {
        this.top1 = top1;
    }

    public Top3________ getTop3() {
        return top3;
    }

    public void setTop3(Top3________ top3) {
        this.top3 = top3;
    }

    public Top5________ getTop5() {
        return top5;
    }

    public void setTop5(Top5________ top5) {
        this.top5 = top5;
    }

    public Top6________ getTop6() {
        return top6;
    }

    public void setTop6(Top6________ top6) {
        this.top6 = top6;
    }

    public Top10________ getTop10() {
        return top10;
    }

    public void setTop10(Top10________ top10) {
        this.top10 = top10;
    }

    public Top12________ getTop12() {
        return top12;
    }

    public void setTop12(Top12________ top12) {
        this.top12 = top12;
    }

    public Top25________ getTop25() {
        return top25;
    }

    public void setTop25(Top25________ top25) {
        this.top25 = top25;
    }

    public Kd________ getKd() {
        return kd;
    }

    public void setKd(Kd________ kd) {
        this.kd = kd;
    }

    public WinRatio________ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio________ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches________ getMatches() {
        return matches;
    }

    public void setMatches(Matches________ matches) {
        this.matches = matches;
    }

    public Kills________ getKills() {
        return kills;
    }

    public void setKills(Kills________ kills) {
        this.kills = kills;
    }

    public Kpg________ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg________ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch________ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch________ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
