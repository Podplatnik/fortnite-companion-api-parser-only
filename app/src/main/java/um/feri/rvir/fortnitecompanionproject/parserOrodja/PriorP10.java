
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PriorP10 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating_______ trnRating;
    @SerializedName("score")
    @Expose
    private Score_______ score;
    @SerializedName("top1")
    @Expose
    private Top1_______ top1;
    @SerializedName("top3")
    @Expose
    private Top3_______ top3;
    @SerializedName("top5")
    @Expose
    private Top5_______ top5;
    @SerializedName("top6")
    @Expose
    private Top6_______ top6;
    @SerializedName("top10")
    @Expose
    private Top10_______ top10;
    @SerializedName("top12")
    @Expose
    private Top12_______ top12;
    @SerializedName("top25")
    @Expose
    private Top25_______ top25;
    @SerializedName("kd")
    @Expose
    private Kd_______ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio_______ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches_______ matches;
    @SerializedName("kills")
    @Expose
    private Kills_______ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg_______ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch_______ scorePerMatch;
    public final static Parcelable.Creator<PriorP10> CREATOR = new Creator<PriorP10>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PriorP10 createFromParcel(Parcel in) {
            return new PriorP10(in);
        }

        public PriorP10 [] newArray(int size) {
            return (new PriorP10[size]);
        }

    }
    ;

    protected PriorP10(Parcel in) {
        this.trnRating = ((TrnRating_______) in.readValue((TrnRating_______.class.getClassLoader())));
        this.score = ((Score_______) in.readValue((Score_______.class.getClassLoader())));
        this.top1 = ((Top1_______) in.readValue((Top1_______.class.getClassLoader())));
        this.top3 = ((Top3_______) in.readValue((Top3_______.class.getClassLoader())));
        this.top5 = ((Top5_______) in.readValue((Top5_______.class.getClassLoader())));
        this.top6 = ((Top6_______) in.readValue((Top6_______.class.getClassLoader())));
        this.top10 = ((Top10_______) in.readValue((Top10_______.class.getClassLoader())));
        this.top12 = ((Top12_______) in.readValue((Top12_______.class.getClassLoader())));
        this.top25 = ((Top25_______) in.readValue((Top25_______.class.getClassLoader())));
        this.kd = ((Kd_______) in.readValue((Kd_______.class.getClassLoader())));
        this.winRatio = ((WinRatio_______) in.readValue((WinRatio_______.class.getClassLoader())));
        this.matches = ((Matches_______) in.readValue((Matches_______.class.getClassLoader())));
        this.kills = ((Kills_______) in.readValue((Kills_______.class.getClassLoader())));
        this.kpg = ((Kpg_______) in.readValue((Kpg_______.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch_______) in.readValue((ScorePerMatch_______.class.getClassLoader())));
    }

    public PriorP10() {
    }

    public TrnRating_______ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating_______ trnRating) {
        this.trnRating = trnRating;
    }

    public Score_______ getScore() {
        return score;
    }

    public void setScore(Score_______ score) {
        this.score = score;
    }

    public Top1_______ getTop1() {
        return top1;
    }

    public void setTop1(Top1_______ top1) {
        this.top1 = top1;
    }

    public Top3_______ getTop3() {
        return top3;
    }

    public void setTop3(Top3_______ top3) {
        this.top3 = top3;
    }

    public Top5_______ getTop5() {
        return top5;
    }

    public void setTop5(Top5_______ top5) {
        this.top5 = top5;
    }

    public Top6_______ getTop6() {
        return top6;
    }

    public void setTop6(Top6_______ top6) {
        this.top6 = top6;
    }

    public Top10_______ getTop10() {
        return top10;
    }

    public void setTop10(Top10_______ top10) {
        this.top10 = top10;
    }

    public Top12_______ getTop12() {
        return top12;
    }

    public void setTop12(Top12_______ top12) {
        this.top12 = top12;
    }

    public Top25_______ getTop25() {
        return top25;
    }

    public void setTop25(Top25_______ top25) {
        this.top25 = top25;
    }

    public Kd_______ getKd() {
        return kd;
    }

    public void setKd(Kd_______ kd) {
        this.kd = kd;
    }

    public WinRatio_______ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio_______ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches_______ getMatches() {
        return matches;
    }

    public void setMatches(Matches_______ matches) {
        this.matches = matches;
    }

    public Kills_______ getKills() {
        return kills;
    }

    public void setKills(Kills_______ kills) {
        this.kills = kills;
    }

    public Kpg_______ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg_______ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch_______ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch_______ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
