
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Stats implements Parcelable
{

    @SerializedName("p2")
    @Expose
    private P2 p2;
    @SerializedName("p10")
    @Expose
    private P10 p10;
    @SerializedName("p9")
    @Expose
    private P9 p9;
    @SerializedName("curr_p2")
    @Expose
    private CurrP2 currP2;
    @SerializedName("curr_p10")
    @Expose
    private CurrP10 currP10;
    @SerializedName("curr_p9")
    @Expose
    private CurrP9 currP9;
    @SerializedName("prior_p2")
    @Expose
    private PriorP2 priorP2;
    @SerializedName("prior_p10")
    @Expose
    private PriorP10 priorP10;
    @SerializedName("prior_p9")
    @Expose
    private PriorP9 priorP9;
    public final static Parcelable.Creator<Stats> CREATOR = new Creator<Stats>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Stats createFromParcel(Parcel in) {
            return new Stats(in);
        }

        public Stats[] newArray(int size) {
            return (new Stats[size]);
        }

    }
    ;

    protected Stats(Parcel in) {
        this.p2 = ((P2) in.readValue((P2.class.getClassLoader())));
        this.p10 = ((P10) in.readValue((P10.class.getClassLoader())));
        this.p9 = ((P9) in.readValue((P9.class.getClassLoader())));
        this.currP2 = ((CurrP2) in.readValue((CurrP2.class.getClassLoader())));
        this.currP10 = ((CurrP10) in.readValue((CurrP10.class.getClassLoader())));
        this.currP9 = ((CurrP9) in.readValue((CurrP9.class.getClassLoader())));
        this.priorP2 = ((PriorP2) in.readValue((PriorP2.class.getClassLoader())));
        this.priorP10 = ((PriorP10) in.readValue((PriorP10.class.getClassLoader())));
        this.priorP9 = ((PriorP9) in.readValue((PriorP9.class.getClassLoader())));
    }

    public Stats() {
    }

    public P2 getP2() {
        return p2;
    }

    public void setP2(P2 p2) {
        this.p2 = p2;
    }

    public P10 getP10() {
        return p10;
    }

    public void setP10(P10 p10) {
        this.p10 = p10;
    }

    public P9 getP9() {
        return p9;
    }

    public void setP9(P9 p9) {
        this.p9 = p9;
    }

    public CurrP2 getCurrP2() {
        return currP2;
    }

    public void setCurrP2(CurrP2 currP2) {
        this.currP2 = currP2;
    }

    public CurrP10 getCurrP10() {
        return currP10;
    }

    public void setCurrP10(CurrP10 currP10) {
        this.currP10 = currP10;
    }

    public CurrP9 getCurrP9() {
        return currP9;
    }

    public void setCurrP9(CurrP9 currP9) {
        this.currP9 = currP9;
    }

    public PriorP2 getPriorP2() {
        return priorP2;
    }

    public void setPriorP2(PriorP2 priorP2) {
        this.priorP2 = priorP2;
    }

    public PriorP10 getPriorP10() {
        return priorP10;
    }

    public void setPriorP10(PriorP10 priorP10) {
        this.priorP10 = priorP10;
    }

    public PriorP9 getPriorP9() {
        return priorP9;
    }

    public void setPriorP9(PriorP9 priorP9) {
        this.priorP9 = priorP9;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("p2", p2).append("p10", p10).append("p9", p9).append("currP2", currP2).append("currP10", currP10).append("currP9", currP9).append("priorP2", priorP2).append("priorP10", priorP10).append("priorP9", priorP9).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(p2);
        dest.writeValue(p10);
        dest.writeValue(p9);
        dest.writeValue(currP2);
        dest.writeValue(currP10);
        dest.writeValue(currP9);
        dest.writeValue(priorP2);
        dest.writeValue(priorP10);
        dest.writeValue(priorP9);
    }

    public int describeContents() {
        return  0;
    }

}
