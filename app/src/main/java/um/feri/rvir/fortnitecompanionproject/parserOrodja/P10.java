
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class P10 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating_ trnRating;
    @SerializedName("score")
    @Expose
    private Score_ score;
    @SerializedName("top1")
    @Expose
    private Top1_ top1;
    @SerializedName("top3")
    @Expose
    private Top3_ top3;
    @SerializedName("top5")
    @Expose
    private Top5_ top5;
    @SerializedName("top6")
    @Expose
    private Top6_ top6;
    @SerializedName("top10")
    @Expose
    private Top10_ top10;
    @SerializedName("top12")
    @Expose
    private Top12_ top12;
    @SerializedName("top25")
    @Expose
    private Top25_ top25;
    @SerializedName("kd")
    @Expose
    private Kd_ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio_ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches_ matches;
    @SerializedName("kills")
    @Expose
    private Kills_ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg_ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch_ scorePerMatch;
    public final static Parcelable.Creator<P10> CREATOR = new Creator<P10>() {


        @SuppressWarnings({
            "unchecked"
        })
        public P10 createFromParcel(Parcel in) {
            return new P10(in);
        }

        public P10 [] newArray(int size) {
            return (new P10[size]);
        }

    }
    ;

    protected P10(Parcel in) {
        this.trnRating = ((TrnRating_) in.readValue((TrnRating_.class.getClassLoader())));
        this.score = ((Score_) in.readValue((Score_.class.getClassLoader())));
        this.top1 = ((Top1_) in.readValue((Top1_.class.getClassLoader())));
        this.top3 = ((Top3_) in.readValue((Top3_.class.getClassLoader())));
        this.top5 = ((Top5_) in.readValue((Top5_.class.getClassLoader())));
        this.top6 = ((Top6_) in.readValue((Top6_.class.getClassLoader())));
        this.top10 = ((Top10_) in.readValue((Top10_.class.getClassLoader())));
        this.top12 = ((Top12_) in.readValue((Top12_.class.getClassLoader())));
        this.top25 = ((Top25_) in.readValue((Top25_.class.getClassLoader())));
        this.kd = ((Kd_) in.readValue((Kd_.class.getClassLoader())));
        this.winRatio = ((WinRatio_) in.readValue((WinRatio_.class.getClassLoader())));
        this.matches = ((Matches_) in.readValue((Matches_.class.getClassLoader())));
        this.kills = ((Kills_) in.readValue((Kills_.class.getClassLoader())));
        this.kpg = ((Kpg_) in.readValue((Kpg_.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch_) in.readValue((ScorePerMatch_.class.getClassLoader())));
    }

    public P10() {
    }

    public TrnRating_ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating_ trnRating) {
        this.trnRating = trnRating;
    }

    public Score_ getScore() {
        return score;
    }

    public void setScore(Score_ score) {
        this.score = score;
    }

    public Top1_ getTop1() {
        return top1;
    }

    public void setTop1(Top1_ top1) {
        this.top1 = top1;
    }

    public Top3_ getTop3() {
        return top3;
    }

    public void setTop3(Top3_ top3) {
        this.top3 = top3;
    }

    public Top5_ getTop5() {
        return top5;
    }

    public void setTop5(Top5_ top5) {
        this.top5 = top5;
    }

    public Top6_ getTop6() {
        return top6;
    }

    public void setTop6(Top6_ top6) {
        this.top6 = top6;
    }

    public Top10_ getTop10() {
        return top10;
    }

    public void setTop10(Top10_ top10) {
        this.top10 = top10;
    }

    public Top12_ getTop12() {
        return top12;
    }

    public void setTop12(Top12_ top12) {
        this.top12 = top12;
    }

    public Top25_ getTop25() {
        return top25;
    }

    public void setTop25(Top25_ top25) {
        this.top25 = top25;
    }

    public Kd_ getKd() {
        return kd;
    }

    public void setKd(Kd_ kd) {
        this.kd = kd;
    }

    public WinRatio_ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio_ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches_ getMatches() {
        return matches;
    }

    public void setMatches(Matches_ matches) {
        this.matches = matches;
    }

    public Kills_ getKills() {
        return kills;
    }

    public void setKills(Kills_ kills) {
        this.kills = kills;
    }

    public Kpg_ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg_ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch_ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch_ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
