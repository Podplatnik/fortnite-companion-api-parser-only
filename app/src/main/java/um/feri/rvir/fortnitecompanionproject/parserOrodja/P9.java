
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class P9 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating__ trnRating;
    @SerializedName("score")
    @Expose
    private Score__ score;
    @SerializedName("top1")
    @Expose
    private Top1__ top1;
    @SerializedName("top3")
    @Expose
    private Top3__ top3;
    @SerializedName("top5")
    @Expose
    private Top5__ top5;
    @SerializedName("top6")
    @Expose
    private Top6__ top6;
    @SerializedName("top10")
    @Expose
    private Top10__ top10;
    @SerializedName("top12")
    @Expose
    private Top12__ top12;
    @SerializedName("top25")
    @Expose
    private Top25__ top25;
    @SerializedName("kd")
    @Expose
    private Kd__ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio__ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches__ matches;
    @SerializedName("kills")
    @Expose
    private Kills__ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg__ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch__ scorePerMatch;
    public final static Parcelable.Creator<P9> CREATOR = new Creator<P9>() {


        @SuppressWarnings({
            "unchecked"
        })
        public P9 createFromParcel(Parcel in) {
            return new P9(in);
        }

        public P9 [] newArray(int size) {
            return (new P9[size]);
        }

    }
    ;

    protected P9(Parcel in) {
        this.trnRating = ((TrnRating__) in.readValue((TrnRating__.class.getClassLoader())));
        this.score = ((Score__) in.readValue((Score__.class.getClassLoader())));
        this.top1 = ((Top1__) in.readValue((Top1__.class.getClassLoader())));
        this.top3 = ((Top3__) in.readValue((Top3__.class.getClassLoader())));
        this.top5 = ((Top5__) in.readValue((Top5__.class.getClassLoader())));
        this.top6 = ((Top6__) in.readValue((Top6__.class.getClassLoader())));
        this.top10 = ((Top10__) in.readValue((Top10__.class.getClassLoader())));
        this.top12 = ((Top12__) in.readValue((Top12__.class.getClassLoader())));
        this.top25 = ((Top25__) in.readValue((Top25__.class.getClassLoader())));
        this.kd = ((Kd__) in.readValue((Kd__.class.getClassLoader())));
        this.winRatio = ((WinRatio__) in.readValue((WinRatio__.class.getClassLoader())));
        this.matches = ((Matches__) in.readValue((Matches__.class.getClassLoader())));
        this.kills = ((Kills__) in.readValue((Kills__.class.getClassLoader())));
        this.kpg = ((Kpg__) in.readValue((Kpg__.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch__) in.readValue((ScorePerMatch__.class.getClassLoader())));
    }

    public P9() {
    }

    public TrnRating__ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating__ trnRating) {
        this.trnRating = trnRating;
    }

    public Score__ getScore() {
        return score;
    }

    public void setScore(Score__ score) {
        this.score = score;
    }

    public Top1__ getTop1() {
        return top1;
    }

    public void setTop1(Top1__ top1) {
        this.top1 = top1;
    }

    public Top3__ getTop3() {
        return top3;
    }

    public void setTop3(Top3__ top3) {
        this.top3 = top3;
    }

    public Top5__ getTop5() {
        return top5;
    }

    public void setTop5(Top5__ top5) {
        this.top5 = top5;
    }

    public Top6__ getTop6() {
        return top6;
    }

    public void setTop6(Top6__ top6) {
        this.top6 = top6;
    }

    public Top10__ getTop10() {
        return top10;
    }

    public void setTop10(Top10__ top10) {
        this.top10 = top10;
    }

    public Top12__ getTop12() {
        return top12;
    }

    public void setTop12(Top12__ top12) {
        this.top12 = top12;
    }

    public Top25__ getTop25() {
        return top25;
    }

    public void setTop25(Top25__ top25) {
        this.top25 = top25;
    }

    public Kd__ getKd() {
        return kd;
    }

    public void setKd(Kd__ kd) {
        this.kd = kd;
    }

    public WinRatio__ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio__ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches__ getMatches() {
        return matches;
    }

    public void setMatches(Matches__ matches) {
        this.matches = matches;
    }

    public Kills__ getKills() {
        return kills;
    }

    public void setKills(Kills__ kills) {
        this.kills = kills;
    }

    public Kpg__ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg__ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch__ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch__ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
