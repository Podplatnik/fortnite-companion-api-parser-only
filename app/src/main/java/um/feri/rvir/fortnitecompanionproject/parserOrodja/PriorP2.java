
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class PriorP2 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating______ trnRating;
    @SerializedName("score")
    @Expose
    private Score______ score;
    @SerializedName("top1")
    @Expose
    private Top1______ top1;
    @SerializedName("top3")
    @Expose
    private Top3______ top3;
    @SerializedName("top5")
    @Expose
    private Top5______ top5;
    @SerializedName("top6")
    @Expose
    private Top6______ top6;
    @SerializedName("top10")
    @Expose
    private Top10______ top10;
    @SerializedName("top12")
    @Expose
    private Top12______ top12;
    @SerializedName("top25")
    @Expose
    private Top25______ top25;
    @SerializedName("kd")
    @Expose
    private Kd______ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio______ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches______ matches;
    @SerializedName("kills")
    @Expose
    private Kills______ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg______ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch______ scorePerMatch;
    public final static Parcelable.Creator<PriorP2> CREATOR = new Creator<PriorP2>() {


        @SuppressWarnings({
            "unchecked"
        })
        public PriorP2 createFromParcel(Parcel in) {
            return new PriorP2(in);
        }

        public PriorP2 [] newArray(int size) {
            return (new PriorP2[size]);
        }

    }
    ;

    protected PriorP2(Parcel in) {
        this.trnRating = ((TrnRating______) in.readValue((TrnRating______.class.getClassLoader())));
        this.score = ((Score______) in.readValue((Score______.class.getClassLoader())));
        this.top1 = ((Top1______) in.readValue((Top1______.class.getClassLoader())));
        this.top3 = ((Top3______) in.readValue((Top3______.class.getClassLoader())));
        this.top5 = ((Top5______) in.readValue((Top5______.class.getClassLoader())));
        this.top6 = ((Top6______) in.readValue((Top6______.class.getClassLoader())));
        this.top10 = ((Top10______) in.readValue((Top10______.class.getClassLoader())));
        this.top12 = ((Top12______) in.readValue((Top12______.class.getClassLoader())));
        this.top25 = ((Top25______) in.readValue((Top25______.class.getClassLoader())));
        this.kd = ((Kd______) in.readValue((Kd______.class.getClassLoader())));
        this.winRatio = ((WinRatio______) in.readValue((WinRatio______.class.getClassLoader())));
        this.matches = ((Matches______) in.readValue((Matches______.class.getClassLoader())));
        this.kills = ((Kills______) in.readValue((Kills______.class.getClassLoader())));
        this.kpg = ((Kpg______) in.readValue((Kpg______.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch______) in.readValue((ScorePerMatch______.class.getClassLoader())));
    }

    public PriorP2() {
    }

    public TrnRating______ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating______ trnRating) {
        this.trnRating = trnRating;
    }

    public Score______ getScore() {
        return score;
    }

    public void setScore(Score______ score) {
        this.score = score;
    }

    public Top1______ getTop1() {
        return top1;
    }

    public void setTop1(Top1______ top1) {
        this.top1 = top1;
    }

    public Top3______ getTop3() {
        return top3;
    }

    public void setTop3(Top3______ top3) {
        this.top3 = top3;
    }

    public Top5______ getTop5() {
        return top5;
    }

    public void setTop5(Top5______ top5) {
        this.top5 = top5;
    }

    public Top6______ getTop6() {
        return top6;
    }

    public void setTop6(Top6______ top6) {
        this.top6 = top6;
    }

    public Top10______ getTop10() {
        return top10;
    }

    public void setTop10(Top10______ top10) {
        this.top10 = top10;
    }

    public Top12______ getTop12() {
        return top12;
    }

    public void setTop12(Top12______ top12) {
        this.top12 = top12;
    }

    public Top25______ getTop25() {
        return top25;
    }

    public void setTop25(Top25______ top25) {
        this.top25 = top25;
    }

    public Kd______ getKd() {
        return kd;
    }

    public void setKd(Kd______ kd) {
        this.kd = kd;
    }

    public WinRatio______ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio______ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches______ getMatches() {
        return matches;
    }

    public void setMatches(Matches______ matches) {
        this.matches = matches;
    }

    public Kills______ getKills() {
        return kills;
    }

    public void setKills(Kills______ kills) {
        this.kills = kills;
    }

    public Kpg______ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg______ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch______ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch______ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
