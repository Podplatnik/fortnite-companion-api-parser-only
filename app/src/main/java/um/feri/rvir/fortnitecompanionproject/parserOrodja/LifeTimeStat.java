
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class LifeTimeStat implements Parcelable
{

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("value")
    @Expose
    private String value;
    public final static Parcelable.Creator<LifeTimeStat> CREATOR = new Creator<LifeTimeStat>() {


        @SuppressWarnings({
            "unchecked"
        })
        public LifeTimeStat createFromParcel(Parcel in) {
            return new LifeTimeStat(in);
        }

        public LifeTimeStat[] newArray(int size) {
            return (new LifeTimeStat[size]);
        }

    }
    ;

    protected LifeTimeStat(Parcel in) {
        this.key = ((String) in.readValue((String.class.getClassLoader())));
        this.value = ((String) in.readValue((String.class.getClassLoader())));
    }

    public LifeTimeStat() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("key", key).append("value", value).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(key);
        dest.writeValue(value);
    }

    public int describeContents() {
        return  0;
    }

}
