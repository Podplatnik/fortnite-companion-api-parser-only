
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class P2 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating trnRating;
    @SerializedName("score")
    @Expose
    private Score score;
    @SerializedName("top1")
    @Expose
    private Top1 top1;
    @SerializedName("top3")
    @Expose
    private Top3 top3;
    @SerializedName("top5")
    @Expose
    private Top5 top5;
    @SerializedName("top6")
    @Expose
    private Top6 top6;
    @SerializedName("top10")
    @Expose
    private Top10 top10;
    @SerializedName("top12")
    @Expose
    private Top12 top12;
    @SerializedName("top25")
    @Expose
    private Top25 top25;
    @SerializedName("kd")
    @Expose
    private Kd kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio winRatio;
    @SerializedName("matches")
    @Expose
    private Matches matches;
    @SerializedName("kills")
    @Expose
    private Kills kills;
    @SerializedName("kpg")
    @Expose
    private Kpg kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch scorePerMatch;
    public final static Parcelable.Creator<P2> CREATOR = new Creator<P2>() {


        @SuppressWarnings({
            "unchecked"
        })
        public P2 createFromParcel(Parcel in) {
            return new P2(in);
        }

        public P2 [] newArray(int size) {
            return (new P2[size]);
        }

    }
    ;

    protected P2(Parcel in) {
        this.trnRating = ((TrnRating) in.readValue((TrnRating.class.getClassLoader())));
        this.score = ((Score) in.readValue((Score.class.getClassLoader())));
        this.top1 = ((Top1) in.readValue((Top1.class.getClassLoader())));
        this.top3 = ((Top3) in.readValue((Top3.class.getClassLoader())));
        this.top5 = ((Top5) in.readValue((Top5.class.getClassLoader())));
        this.top6 = ((Top6) in.readValue((Top6.class.getClassLoader())));
        this.top10 = ((Top10) in.readValue((Top10.class.getClassLoader())));
        this.top12 = ((Top12) in.readValue((Top12.class.getClassLoader())));
        this.top25 = ((Top25) in.readValue((Top25.class.getClassLoader())));
        this.kd = ((Kd) in.readValue((Kd.class.getClassLoader())));
        this.winRatio = ((WinRatio) in.readValue((WinRatio.class.getClassLoader())));
        this.matches = ((Matches) in.readValue((Matches.class.getClassLoader())));
        this.kills = ((Kills) in.readValue((Kills.class.getClassLoader())));
        this.kpg = ((Kpg) in.readValue((Kpg.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch) in.readValue((ScorePerMatch.class.getClassLoader())));
    }

    public P2() {
    }

    public TrnRating getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating trnRating) {
        this.trnRating = trnRating;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public Top1 getTop1() {
        return top1;
    }

    public void setTop1(Top1 top1) {
        this.top1 = top1;
    }

    public Top3 getTop3() {
        return top3;
    }

    public void setTop3(Top3 top3) {
        this.top3 = top3;
    }

    public Top5 getTop5() {
        return top5;
    }

    public void setTop5(Top5 top5) {
        this.top5 = top5;
    }

    public Top6 getTop6() {
        return top6;
    }

    public void setTop6(Top6 top6) {
        this.top6 = top6;
    }

    public Top10 getTop10() {
        return top10;
    }

    public void setTop10(Top10 top10) {
        this.top10 = top10;
    }

    public Top12 getTop12() {
        return top12;
    }

    public void setTop12(Top12 top12) {
        this.top12 = top12;
    }

    public Top25 getTop25() {
        return top25;
    }

    public void setTop25(Top25 top25) {
        this.top25 = top25;
    }

    public Kd getKd() {
        return kd;
    }

    public void setKd(Kd kd) {
        this.kd = kd;
    }

    public WinRatio getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio winRatio) {
        this.winRatio = winRatio;
    }

    public Matches getMatches() {
        return matches;
    }

    public void setMatches(Matches matches) {
        this.matches = matches;
    }

    public Kills getKills() {
        return kills;
    }

    public void setKills(Kills kills) {
        this.kills = kills;
    }

    public Kpg getKpg() {
        return kpg;
    }

    public void setKpg(Kpg kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
