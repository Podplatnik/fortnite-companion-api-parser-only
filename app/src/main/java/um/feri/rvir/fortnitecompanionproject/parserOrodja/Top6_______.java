
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Top6_______ implements Parcelable
{

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("field")
    @Expose
    private String field;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("valueInt")
    @Expose
    private Integer valueInt;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("displayValue")
    @Expose
    private String displayValue;
    public final static Parcelable.Creator<Top6_______> CREATOR = new Creator<Top6_______>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Top6_______ createFromParcel(Parcel in) {
            return new Top6_______(in);
        }

        public Top6_______[] newArray(int size) {
            return (new Top6_______[size]);
        }

    }
    ;

    protected Top6_______(Parcel in) {
        this.label = ((String) in.readValue((String.class.getClassLoader())));
        this.field = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((String) in.readValue((String.class.getClassLoader())));
        this.valueInt = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.value = ((String) in.readValue((String.class.getClassLoader())));
        this.displayValue = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Top6_______() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getValueInt() {
        return valueInt;
    }

    public void setValueInt(Integer valueInt) {
        this.valueInt = valueInt;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("label", label).append("field", field).append("category", category).append("valueInt", valueInt).append("value", value).append("displayValue", displayValue).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(field);
        dest.writeValue(category);
        dest.writeValue(valueInt);
        dest.writeValue(value);
        dest.writeValue(displayValue);
    }

    public int describeContents() {
        return  0;
    }

}
