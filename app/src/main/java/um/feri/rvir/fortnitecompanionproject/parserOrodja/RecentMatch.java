
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class RecentMatch implements Parcelable
{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("accountId")
    @Expose
    private String accountId;
    @SerializedName("playlist")
    @Expose
    private String playlist;
    @SerializedName("kills")
    @Expose
    private Integer kills;
    @SerializedName("minutesPlayed")
    @Expose
    private Integer minutesPlayed;
    @SerializedName("top1")
    @Expose
    private Integer top1;
    @SerializedName("top5")
    @Expose
    private Integer top5;
    @SerializedName("top6")
    @Expose
    private Integer top6;
    @SerializedName("top10")
    @Expose
    private Integer top10;
    @SerializedName("top12")
    @Expose
    private Integer top12;
    @SerializedName("top25")
    @Expose
    private Integer top25;
    @SerializedName("matches")
    @Expose
    private Integer matches;
    @SerializedName("top3")
    @Expose
    private Integer top3;
    @SerializedName("dateCollected")
    @Expose
    private String dateCollected;
    @SerializedName("score")
    @Expose
    private Integer score;
    @SerializedName("platform")
    @Expose
    private Integer platform;
    @SerializedName("trnRating")
    @Expose
    private Double trnRating;
    @SerializedName("trnRatingChange")
    @Expose
    private Double trnRatingChange;
    public final static Parcelable.Creator<RecentMatch> CREATOR = new Creator<RecentMatch>() {


        @SuppressWarnings({
            "unchecked"
        })
        public RecentMatch createFromParcel(Parcel in) {
            return new RecentMatch(in);
        }

        public RecentMatch[] newArray(int size) {
            return (new RecentMatch[size]);
        }

    }
    ;

    protected RecentMatch(Parcel in) {
        this.id = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.accountId = ((String) in.readValue((String.class.getClassLoader())));
        this.playlist = ((String) in.readValue((String.class.getClassLoader())));
        this.kills = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.minutesPlayed = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top1 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top5 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top6 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top10 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top12 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top25 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.matches = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.top3 = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.dateCollected = ((String) in.readValue((String.class.getClassLoader())));
        this.score = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.platform = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.trnRating = ((Double) in.readValue((Double.class.getClassLoader())));
        this.trnRatingChange = ((Double) in.readValue((Double.class.getClassLoader())));
    }

    public RecentMatch() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPlaylist() {
        return playlist;
    }

    public void setPlaylist(String playlist) {
        this.playlist = playlist;
    }

    public Integer getKills() {
        return kills;
    }

    public void setKills(Integer kills) {
        this.kills = kills;
    }

    public Integer getMinutesPlayed() {
        return minutesPlayed;
    }

    public void setMinutesPlayed(Integer minutesPlayed) {
        this.minutesPlayed = minutesPlayed;
    }

    public Integer getTop1() {
        return top1;
    }

    public void setTop1(Integer top1) {
        this.top1 = top1;
    }

    public Integer getTop5() {
        return top5;
    }

    public void setTop5(Integer top5) {
        this.top5 = top5;
    }

    public Integer getTop6() {
        return top6;
    }

    public void setTop6(Integer top6) {
        this.top6 = top6;
    }

    public Integer getTop10() {
        return top10;
    }

    public void setTop10(Integer top10) {
        this.top10 = top10;
    }

    public Integer getTop12() {
        return top12;
    }

    public void setTop12(Integer top12) {
        this.top12 = top12;
    }

    public Integer getTop25() {
        return top25;
    }

    public void setTop25(Integer top25) {
        this.top25 = top25;
    }

    public Integer getMatches() {
        return matches;
    }

    public void setMatches(Integer matches) {
        this.matches = matches;
    }

    public Integer getTop3() {
        return top3;
    }

    public void setTop3(Integer top3) {
        this.top3 = top3;
    }

    public String getDateCollected() {
        return dateCollected;
    }

    public void setDateCollected(String dateCollected) {
        this.dateCollected = dateCollected;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getPlatform() {
        return platform;
    }

    public void setPlatform(Integer platform) {
        this.platform = platform;
    }

    public Double getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(Double trnRating) {
        this.trnRating = trnRating;
    }

    public Double getTrnRatingChange() {
        return trnRatingChange;
    }

    public void setTrnRatingChange(Double trnRatingChange) {
        this.trnRatingChange = trnRatingChange;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("accountId", accountId).append("playlist", playlist).append("kills", kills).append("minutesPlayed", minutesPlayed).append("top1", top1).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("matches", matches).append("top3", top3).append("dateCollected", dateCollected).append("score", score).append("platform", platform).append("trnRating", trnRating).append("trnRatingChange", trnRatingChange).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(accountId);
        dest.writeValue(playlist);
        dest.writeValue(kills);
        dest.writeValue(minutesPlayed);
        dest.writeValue(top1);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(matches);
        dest.writeValue(top3);
        dest.writeValue(dateCollected);
        dest.writeValue(score);
        dest.writeValue(platform);
        dest.writeValue(trnRating);
        dest.writeValue(trnRatingChange);
    }

    public int describeContents() {
        return  0;
    }

}
