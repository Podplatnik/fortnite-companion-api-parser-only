package um.feri.rvir.fortnitecompanionproject;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import um.feri.rvir.fortnitecompanionproject.parserOrodja.Igralec;
import um.feri.rvir.fortnitecompanionproject.parserOrodja.IgralecSimple;
import um.feri.rvir.fortnitecompanionproject.parserOrodja.IgralecSimplifier;


public class MainActivity extends AppCompatActivity {

    private TextView textViewStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewStatus = findViewById(R.id.textViewStatus);
    }

    public void poizveduj(View view) {

        final String IGRALEC = "Price4534";

        PoizvedujPoIgralcuTash task = new PoizvedujPoIgralcuTash();
        task.execute(IGRALEC);

    }

    private class PoizvedujPoIgralcuTash extends AsyncTask<String, Integer, Object> {

        @Override
        protected Object doInBackground(String... strings) {
            try {
                OkHttpClient client = new OkHttpClient();

                Request request = new Request.Builder()
                        .header("TRN-Api-Key", "7e63c718-ce95-41bb-a9fa-231358040752")
                        .url("https://api.fortnitetracker.com/v1/profile/pc/"+strings[0])
                        .build();

                Response response = client.newCall(request).execute();
                String jsonData = response.body().string();
                //System.out.println(jsonData);


                Igralec igralecReal = new Gson().fromJson(jsonData, Igralec.class);
                IgralecSimple igralecPreprosti = IgralecSimplifier.vPreprostegaIgralca(igralecReal);
                System.out.println(igralecPreprosti);
                return Integer.toString(response.code());
            }catch (Exception e){
                Log.e("NAPAKA", e.getMessage());
                e.printStackTrace();
                return "Neuspelo...";
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            String koda = (String) o;
            textViewStatus.setText(koda);
        }
    }
}
