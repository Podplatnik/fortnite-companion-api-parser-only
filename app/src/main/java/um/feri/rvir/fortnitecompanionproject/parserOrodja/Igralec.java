
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Igralec implements Parcelable
{

    @SerializedName("accountId")
    @Expose
    private String accountId;
    @SerializedName("platformId")
    @Expose
    private Integer platformId;
    @SerializedName("platformName")
    @Expose
    private String platformName;
    @SerializedName("platformNameLong")
    @Expose
    private String platformNameLong;
    @SerializedName("epicUserHandle")
    @Expose
    private String epicUserHandle;
    @SerializedName("stats")
    @Expose
    private Stats stats;
    @SerializedName("lifeTimeStats")
    @Expose
    private List<LifeTimeStat> lifeTimeStats = null;
    @SerializedName("recentMatches")
    @Expose
    private List<RecentMatch> recentMatches = null;
    public final static Parcelable.Creator<Igralec> CREATOR = new Creator<Igralec>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Igralec createFromParcel(Parcel in) {
            return new Igralec(in);
        }

        public Igralec[] newArray(int size) {
            return (new Igralec[size]);
        }

    }
    ;

    protected Igralec(Parcel in) {
        this.accountId = ((String) in.readValue((String.class.getClassLoader())));
        this.platformId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.platformName = ((String) in.readValue((String.class.getClassLoader())));
        this.platformNameLong = ((String) in.readValue((String.class.getClassLoader())));
        this.epicUserHandle = ((String) in.readValue((String.class.getClassLoader())));
        this.stats = ((Stats) in.readValue((Stats.class.getClassLoader())));
        in.readList(this.lifeTimeStats, (um.feri.rvir.fortnitecompanionproject.parserOrodja.LifeTimeStat.class.getClassLoader()));
        in.readList(this.recentMatches, (um.feri.rvir.fortnitecompanionproject.parserOrodja.RecentMatch.class.getClassLoader()));
    }

    public Igralec() {
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public Integer getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Integer platformId) {
        this.platformId = platformId;
    }

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public String getPlatformNameLong() {
        return platformNameLong;
    }

    public void setPlatformNameLong(String platformNameLong) {
        this.platformNameLong = platformNameLong;
    }

    public String getEpicUserHandle() {
        return epicUserHandle;
    }

    public void setEpicUserHandle(String epicUserHandle) {
        this.epicUserHandle = epicUserHandle;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public List<LifeTimeStat> getLifeTimeStats() {
        return lifeTimeStats;
    }

    public void setLifeTimeStats(List<LifeTimeStat> lifeTimeStats) {
        this.lifeTimeStats = lifeTimeStats;
    }

    public List<RecentMatch> getRecentMatches() {
        return recentMatches;
    }

    public void setRecentMatches(List<RecentMatch> recentMatches) {
        this.recentMatches = recentMatches;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("accountId", accountId).append("platformId", platformId).append("platformName", platformName).append("platformNameLong", platformNameLong).append("epicUserHandle", epicUserHandle).append("stats", stats).append("lifeTimeStats", lifeTimeStats).append("recentMatches", recentMatches).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(accountId);
        dest.writeValue(platformId);
        dest.writeValue(platformName);
        dest.writeValue(platformNameLong);
        dest.writeValue(epicUserHandle);
        dest.writeValue(stats);
        dest.writeList(lifeTimeStats);
        dest.writeList(recentMatches);
    }

    public int describeContents() {
        return  0;
    }

}
