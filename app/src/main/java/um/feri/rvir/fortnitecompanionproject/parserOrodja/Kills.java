
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Kills implements Parcelable
{

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("field")
    @Expose
    private String field;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("valueInt")
    @Expose
    private Integer valueInt;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("rank")
    @Expose
    private Integer rank;
    @SerializedName("percentile")
    @Expose
    private Double percentile;
    @SerializedName("displayValue")
    @Expose
    private String displayValue;
    public final static Parcelable.Creator<Kills> CREATOR = new Creator<Kills>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Kills createFromParcel(Parcel in) {
            return new Kills(in);
        }

        public Kills[] newArray(int size) {
            return (new Kills[size]);
        }

    }
    ;

    protected Kills(Parcel in) {
        this.label = ((String) in.readValue((String.class.getClassLoader())));
        this.field = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((String) in.readValue((String.class.getClassLoader())));
        this.valueInt = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.value = ((String) in.readValue((String.class.getClassLoader())));
        this.rank = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.percentile = ((Double) in.readValue((Double.class.getClassLoader())));
        this.displayValue = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Kills() {
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Integer getValueInt() {
        return valueInt;
    }

    public void setValueInt(Integer valueInt) {
        this.valueInt = valueInt;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Double getPercentile() {
        return percentile;
    }

    public void setPercentile(Double percentile) {
        this.percentile = percentile;
    }

    public String getDisplayValue() {
        return displayValue;
    }

    public void setDisplayValue(String displayValue) {
        this.displayValue = displayValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("label", label).append("field", field).append("category", category).append("valueInt", valueInt).append("value", value).append("rank", rank).append("percentile", percentile).append("displayValue", displayValue).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(label);
        dest.writeValue(field);
        dest.writeValue(category);
        dest.writeValue(valueInt);
        dest.writeValue(value);
        dest.writeValue(rank);
        dest.writeValue(percentile);
        dest.writeValue(displayValue);
    }

    public int describeContents() {
        return  0;
    }

}
