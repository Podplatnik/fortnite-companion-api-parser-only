package um.feri.rvir.fortnitecompanionproject.parserOrodja;

/**
 * Created by Miha on 16/06/2018.
 */

public class IgralecSimplifier {
    public static IgralecSimple vPreprostegaIgralca(Igralec igralecFull){
        IgralecSimple igralecSimple = new IgralecSimple();

        //Overall
        igralecSimple.setAccountId(igralecFull.getAccountId());
        igralecSimple.setPlatformId(igralecFull.getPlatformId());
        igralecSimple.setPlatformName(igralecFull.getPlatformName());
        igralecSimple.setPlatformNameLong(igralecFull.getPlatformNameLong());
        igralecSimple.setEpicUserHandle(igralecFull.getEpicUserHandle());

        //Solo
        igralecSimple.setSoloTRNRating(igralecFull.getStats().getP2().getTrnRating().getValueInt());
        igralecSimple.setSoloTRNRatingRank(igralecFull.getStats().getP2().getTrnRating().getRank());
        igralecSimple.setSoloScore(igralecFull.getStats().getP2().getScore().getValueInt());
        igralecSimple.setSoloScoreRank(igralecFull.getStats().getP2().getScore().getRank());
        igralecSimple.setSoloWins(igralecFull.getStats().getP2().getTop1().getValueInt());
        igralecSimple.setSoloWinsRank(igralecFull.getStats().getP2().getTop1().getRank());
        igralecSimple.setSoloTop10(igralecFull.getStats().getP2().getTop10().getValueInt());
        igralecSimple.setSoloTop25(igralecFull.getStats().getP2().getTop25().getValueInt());
        igralecSimple.setSoloKillDeath(igralecFull.getStats().getP2().getKd().getValueDec());
        igralecSimple.setSoloKillDeathRank(igralecFull.getStats().getP2().getKd().getRank());
        igralecSimple.setSoloWinRatio(igralecFull.getStats().getP2().getWinRatio().getValueDec());
        igralecSimple.setSoloWinRatioRank(igralecFull.getStats().getP2().getWinRatio().getRank());
        igralecSimple.setSoloMatches(igralecFull.getStats().getP2().getMatches().getValueInt());
        igralecSimple.setSoloKills(igralecFull.getStats().getP2().getKills().getValueInt());
        igralecSimple.setSoloKillsRank(igralecFull.getStats().getP2().getKills().getRank());
        igralecSimple.setSoloKillsPerMatch(igralecFull.getStats().getP2().getKpg().getValueDec());
        igralecSimple.setSoloKillsPerMatchRank(igralecFull.getStats().getP2().getKpg().getRank());
        igralecSimple.setSoloScorePerMatch(igralecFull.getStats().getP2().getScorePerMatch().getValueDec());
        igralecSimple.setSoloScorePerMatchRank(igralecFull.getStats().getP2().getScorePerMatch().getRank());

        igralecSimple.setDuoTRNRating(igralecFull.getStats().getP10().getTrnRating().getValueInt());
        igralecSimple.setDuoTRNRatingRank(igralecFull.getStats().getP10().getTrnRating().getRank());
        igralecSimple.setDuoScore(igralecFull.getStats().getP10().getScore().getValueInt());
        igralecSimple.setDuoScoreRank(igralecFull.getStats().getP10().getScore().getRank());
        igralecSimple.setDuoWins(igralecFull.getStats().getP10().getTop1().getValueInt());
        igralecSimple.setDuoWinsRank(igralecFull.getStats().getP10().getTop1().getRank());
        igralecSimple.setDuoTop5(igralecFull.getStats().getP10().getTop5().getValueInt());
        igralecSimple.setDuoTop12(igralecFull.getStats().getP10().getTop12().getValueInt());
        igralecSimple.setDuoKillDeath(igralecFull.getStats().getP10().getKd().getValueDec());
        igralecSimple.setDuoKillDeathRank(igralecFull.getStats().getP10().getKd().getRank());
        igralecSimple.setDuoWinRatio(igralecFull.getStats().getP10().getWinRatio().getValueDec());
        igralecSimple.setDuoWinRatioRank(igralecFull.getStats().getP10().getWinRatio().getRank());
        igralecSimple.setDuoMatches(igralecFull.getStats().getP10().getMatches().getValueInt());
        igralecSimple.setDuoKills(igralecFull.getStats().getP10().getKills().getValueInt());
        igralecSimple.setDuoKillsRank(igralecFull.getStats().getP10().getKills().getRank());
        igralecSimple.setDuoKillsPerMatch(igralecFull.getStats().getP10().getKpg().getValueDec());
        igralecSimple.setDuoKillsPerMatchRank(igralecFull.getStats().getP10().getKpg().getRank());
        igralecSimple.setDuoScorePerMatch(igralecFull.getStats().getP10().getScorePerMatch().getValueDec());
        igralecSimple.setDuoScorePerMatchRank(igralecFull.getStats().getP10().getScorePerMatch().getRank());

        igralecSimple.setSquadTRNRating(igralecFull.getStats().getP9().getTrnRating().getValueInt());
        igralecSimple.setSquadTRNRatingRank(igralecFull.getStats().getP9().getTrnRating().getRank());
        igralecSimple.setSquadScore(igralecFull.getStats().getP9().getScore().getValueInt());
        igralecSimple.setSquadScoreRank(igralecFull.getStats().getP9().getScore().getRank());
        igralecSimple.setSquadWins(igralecFull.getStats().getP9().getTop1().getValueInt());
        igralecSimple.setSquadWinsRank(igralecFull.getStats().getP9().getTop1().getRank());
        igralecSimple.setSquadTop3(igralecFull.getStats().getP9().getTop3().getValueInt());
        igralecSimple.setSquadTop6  (igralecFull.getStats().getP9().getTop6().getValueInt());
        igralecSimple.setSquadKillDeath(igralecFull.getStats().getP9().getKd().getValueDec());
        igralecSimple.setSquadKillDeathRank(igralecFull.getStats().getP9().getKd().getRank());
        igralecSimple.setSquadWinRatio(igralecFull.getStats().getP9().getWinRatio().getValueDec());
        igralecSimple.setSquadWinRatioRank(igralecFull.getStats().getP9().getWinRatio().getRank());
        igralecSimple.setSquadMatches(igralecFull.getStats().getP9().getMatches().getValueInt());
        igralecSimple.setSquadKills(igralecFull.getStats().getP9().getKills().getValueInt());
        igralecSimple.setSquadKillsRank(igralecFull.getStats().getP9().getKills().getRank());
        igralecSimple.setSquadKillsPerMatch(igralecFull.getStats().getP9().getKpg().getValueDec());
        igralecSimple.setSquadKillsPerMatchRank(igralecFull.getStats().getP9().getKpg().getRank());
        igralecSimple.setSquadScorePerMatch(igralecFull.getStats().getP9().getScorePerMatch().getValueDec());
        igralecSimple.setSquadScorePerMatchRank(igralecFull.getStats().getP9().getScorePerMatch().getRank());

        igralecSimple.setLifeTimeScore(igralecFull.getLifeTimeStats().get(6).getValue());
        igralecSimple.setLifeTimeMatchesPlayed(igralecFull.getLifeTimeStats().get(7).getValue());
        igralecSimple.setLifeTimeWins(igralecFull.getLifeTimeStats().get(8).getValue());
        igralecSimple.setLifeTimeWinsProcent(igralecFull.getLifeTimeStats().get(9).getValue());
        igralecSimple.setLifeTimeKills(igralecFull.getLifeTimeStats().get(10).getValue());
        igralecSimple.setLifeTimeKillDeath(igralecFull.getLifeTimeStats().get(11).getValue());




        return igralecSimple;
    }
}
