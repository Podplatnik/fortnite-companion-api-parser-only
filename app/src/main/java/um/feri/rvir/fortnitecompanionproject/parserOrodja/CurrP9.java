
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CurrP9 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating_____ trnRating;
    @SerializedName("score")
    @Expose
    private Score_____ score;
    @SerializedName("top1")
    @Expose
    private Top1_____ top1;
    @SerializedName("top3")
    @Expose
    private Top3_____ top3;
    @SerializedName("top5")
    @Expose
    private Top5_____ top5;
    @SerializedName("top6")
    @Expose
    private Top6_____ top6;
    @SerializedName("top10")
    @Expose
    private Top10_____ top10;
    @SerializedName("top12")
    @Expose
    private Top12_____ top12;
    @SerializedName("top25")
    @Expose
    private Top25_____ top25;
    @SerializedName("kd")
    @Expose
    private Kd_____ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio_____ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches_____ matches;
    @SerializedName("kills")
    @Expose
    private Kills_____ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg_____ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch_____ scorePerMatch;
    public final static Parcelable.Creator<CurrP9> CREATOR = new Creator<CurrP9>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CurrP9 createFromParcel(Parcel in) {
            return new CurrP9(in);
        }

        public CurrP9 [] newArray(int size) {
            return (new CurrP9[size]);
        }

    }
    ;

    protected CurrP9(Parcel in) {
        this.trnRating = ((TrnRating_____) in.readValue((TrnRating_____.class.getClassLoader())));
        this.score = ((Score_____) in.readValue((Score_____.class.getClassLoader())));
        this.top1 = ((Top1_____) in.readValue((Top1_____.class.getClassLoader())));
        this.top3 = ((Top3_____) in.readValue((Top3_____.class.getClassLoader())));
        this.top5 = ((Top5_____) in.readValue((Top5_____.class.getClassLoader())));
        this.top6 = ((Top6_____) in.readValue((Top6_____.class.getClassLoader())));
        this.top10 = ((Top10_____) in.readValue((Top10_____.class.getClassLoader())));
        this.top12 = ((Top12_____) in.readValue((Top12_____.class.getClassLoader())));
        this.top25 = ((Top25_____) in.readValue((Top25_____.class.getClassLoader())));
        this.kd = ((Kd_____) in.readValue((Kd_____.class.getClassLoader())));
        this.winRatio = ((WinRatio_____) in.readValue((WinRatio_____.class.getClassLoader())));
        this.matches = ((Matches_____) in.readValue((Matches_____.class.getClassLoader())));
        this.kills = ((Kills_____) in.readValue((Kills_____.class.getClassLoader())));
        this.kpg = ((Kpg_____) in.readValue((Kpg_____.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch_____) in.readValue((ScorePerMatch_____.class.getClassLoader())));
    }

    public CurrP9() {
    }

    public TrnRating_____ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating_____ trnRating) {
        this.trnRating = trnRating;
    }

    public Score_____ getScore() {
        return score;
    }

    public void setScore(Score_____ score) {
        this.score = score;
    }

    public Top1_____ getTop1() {
        return top1;
    }

    public void setTop1(Top1_____ top1) {
        this.top1 = top1;
    }

    public Top3_____ getTop3() {
        return top3;
    }

    public void setTop3(Top3_____ top3) {
        this.top3 = top3;
    }

    public Top5_____ getTop5() {
        return top5;
    }

    public void setTop5(Top5_____ top5) {
        this.top5 = top5;
    }

    public Top6_____ getTop6() {
        return top6;
    }

    public void setTop6(Top6_____ top6) {
        this.top6 = top6;
    }

    public Top10_____ getTop10() {
        return top10;
    }

    public void setTop10(Top10_____ top10) {
        this.top10 = top10;
    }

    public Top12_____ getTop12() {
        return top12;
    }

    public void setTop12(Top12_____ top12) {
        this.top12 = top12;
    }

    public Top25_____ getTop25() {
        return top25;
    }

    public void setTop25(Top25_____ top25) {
        this.top25 = top25;
    }

    public Kd_____ getKd() {
        return kd;
    }

    public void setKd(Kd_____ kd) {
        this.kd = kd;
    }

    public WinRatio_____ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio_____ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches_____ getMatches() {
        return matches;
    }

    public void setMatches(Matches_____ matches) {
        this.matches = matches;
    }

    public Kills_____ getKills() {
        return kills;
    }

    public void setKills(Kills_____ kills) {
        this.kills = kills;
    }

    public Kpg_____ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg_____ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch_____ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch_____ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
