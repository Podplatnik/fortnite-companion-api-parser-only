
package um.feri.rvir.fortnitecompanionproject.parserOrodja;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class CurrP10 implements Parcelable
{

    @SerializedName("trnRating")
    @Expose
    private TrnRating____ trnRating;
    @SerializedName("score")
    @Expose
    private Score____ score;
    @SerializedName("top1")
    @Expose
    private Top1____ top1;
    @SerializedName("top3")
    @Expose
    private Top3____ top3;
    @SerializedName("top5")
    @Expose
    private Top5____ top5;
    @SerializedName("top6")
    @Expose
    private Top6____ top6;
    @SerializedName("top10")
    @Expose
    private Top10____ top10;
    @SerializedName("top12")
    @Expose
    private Top12____ top12;
    @SerializedName("top25")
    @Expose
    private Top25____ top25;
    @SerializedName("kd")
    @Expose
    private Kd____ kd;
    @SerializedName("winRatio")
    @Expose
    private WinRatio____ winRatio;
    @SerializedName("matches")
    @Expose
    private Matches____ matches;
    @SerializedName("kills")
    @Expose
    private Kills____ kills;
    @SerializedName("kpg")
    @Expose
    private Kpg____ kpg;
    @SerializedName("scorePerMatch")
    @Expose
    private ScorePerMatch____ scorePerMatch;
    public final static Parcelable.Creator<CurrP10> CREATOR = new Creator<CurrP10>() {


        @SuppressWarnings({
            "unchecked"
        })
        public CurrP10 createFromParcel(Parcel in) {
            return new CurrP10(in);
        }

        public CurrP10 [] newArray(int size) {
            return (new CurrP10[size]);
        }

    }
    ;

    protected CurrP10(Parcel in) {
        this.trnRating = ((TrnRating____) in.readValue((TrnRating____.class.getClassLoader())));
        this.score = ((Score____) in.readValue((Score____.class.getClassLoader())));
        this.top1 = ((Top1____) in.readValue((Top1____.class.getClassLoader())));
        this.top3 = ((Top3____) in.readValue((Top3____.class.getClassLoader())));
        this.top5 = ((Top5____) in.readValue((Top5____.class.getClassLoader())));
        this.top6 = ((Top6____) in.readValue((Top6____.class.getClassLoader())));
        this.top10 = ((Top10____) in.readValue((Top10____.class.getClassLoader())));
        this.top12 = ((Top12____) in.readValue((Top12____.class.getClassLoader())));
        this.top25 = ((Top25____) in.readValue((Top25____.class.getClassLoader())));
        this.kd = ((Kd____) in.readValue((Kd____.class.getClassLoader())));
        this.winRatio = ((WinRatio____) in.readValue((WinRatio____.class.getClassLoader())));
        this.matches = ((Matches____) in.readValue((Matches____.class.getClassLoader())));
        this.kills = ((Kills____) in.readValue((Kills____.class.getClassLoader())));
        this.kpg = ((Kpg____) in.readValue((Kpg____.class.getClassLoader())));
        this.scorePerMatch = ((ScorePerMatch____) in.readValue((ScorePerMatch____.class.getClassLoader())));
    }

    public CurrP10() {
    }

    public TrnRating____ getTrnRating() {
        return trnRating;
    }

    public void setTrnRating(TrnRating____ trnRating) {
        this.trnRating = trnRating;
    }

    public Score____ getScore() {
        return score;
    }

    public void setScore(Score____ score) {
        this.score = score;
    }

    public Top1____ getTop1() {
        return top1;
    }

    public void setTop1(Top1____ top1) {
        this.top1 = top1;
    }

    public Top3____ getTop3() {
        return top3;
    }

    public void setTop3(Top3____ top3) {
        this.top3 = top3;
    }

    public Top5____ getTop5() {
        return top5;
    }

    public void setTop5(Top5____ top5) {
        this.top5 = top5;
    }

    public Top6____ getTop6() {
        return top6;
    }

    public void setTop6(Top6____ top6) {
        this.top6 = top6;
    }

    public Top10____ getTop10() {
        return top10;
    }

    public void setTop10(Top10____ top10) {
        this.top10 = top10;
    }

    public Top12____ getTop12() {
        return top12;
    }

    public void setTop12(Top12____ top12) {
        this.top12 = top12;
    }

    public Top25____ getTop25() {
        return top25;
    }

    public void setTop25(Top25____ top25) {
        this.top25 = top25;
    }

    public Kd____ getKd() {
        return kd;
    }

    public void setKd(Kd____ kd) {
        this.kd = kd;
    }

    public WinRatio____ getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(WinRatio____ winRatio) {
        this.winRatio = winRatio;
    }

    public Matches____ getMatches() {
        return matches;
    }

    public void setMatches(Matches____ matches) {
        this.matches = matches;
    }

    public Kills____ getKills() {
        return kills;
    }

    public void setKills(Kills____ kills) {
        this.kills = kills;
    }

    public Kpg____ getKpg() {
        return kpg;
    }

    public void setKpg(Kpg____ kpg) {
        this.kpg = kpg;
    }

    public ScorePerMatch____ getScorePerMatch() {
        return scorePerMatch;
    }

    public void setScorePerMatch(ScorePerMatch____ scorePerMatch) {
        this.scorePerMatch = scorePerMatch;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("trnRating", trnRating).append("score", score).append("top1", top1).append("top3", top3).append("top5", top5).append("top6", top6).append("top10", top10).append("top12", top12).append("top25", top25).append("kd", kd).append("winRatio", winRatio).append("matches", matches).append("kills", kills).append("kpg", kpg).append("scorePerMatch", scorePerMatch).toString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(trnRating);
        dest.writeValue(score);
        dest.writeValue(top1);
        dest.writeValue(top3);
        dest.writeValue(top5);
        dest.writeValue(top6);
        dest.writeValue(top10);
        dest.writeValue(top12);
        dest.writeValue(top25);
        dest.writeValue(kd);
        dest.writeValue(winRatio);
        dest.writeValue(matches);
        dest.writeValue(kills);
        dest.writeValue(kpg);
        dest.writeValue(scorePerMatch);
    }

    public int describeContents() {
        return  0;
    }

}
